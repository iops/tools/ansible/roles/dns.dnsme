# description
Ansible role for DNS management using the `dnsmadeeasy` provider.

# features
Current implementation status:
- deployment of single records (run_mode: `deploy`)
- removal of single records (run_mode: `remove`)


# usage (example)

### add role to your project using ansible-galaxy
ensure the required version is included in your `roles/requirements.yml` file:
```yaml
# DNS management
- name: "dns.dnsme"
  version: "master"
  src: "https://gitlab.com/iops/ansible/roles/dns.dnsme.git"
  scm: 'git'
```


### download/update role via ansible-galaxy
```bash
ansible-galaxy install --force -r roles/requirements.yml --roles-path roles/
```


### use the role in your project's playbooks
check [defaults/main.yml](defaults/main.yml) for list of `required variables`.
```yaml
- hosts: 'webservers'
  gather_facts: yes

  tasks:

    # create/update DNS A record
    - name: 'dns.manage.host.a.record'
      include_role:
        name: 'dns.dnsme'
      vars:
        run_mode: 'deploy'
        record_name: '{{ ansible_fqdn }}'
        record_type: 'A'
        record_value: '{{ ansible_eth0.ipv4.address }}'

    # delete DNS A record
    - name: 'dns.remove.host.a.record'
      include_role:
        name: 'dns.dnsme'
      vars:
        run_mode: 'remove'
        record_name: '{{ ansible_fqdn }}'
        record_type: 'A'
        #optional: record_value: '{{ ansible_eth0.ipv4.address }}'
```
